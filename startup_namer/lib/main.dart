import 'package:english_words/english_words.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white
      ),
        debugShowCheckedModeBanner: false,
        home: RandomWords()
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  State createState() => RandomWordsState();
}

///  This indicates that we’re using the generic State class.
///  Most of the app’s logic and state resides here—
///  it maintains the state for the RandomWords widget.
///  This class saves the generated word pairs.
///  Favorite word pairs (in part 2), as the user adds or removes them from the list
///  by toggling the heart icon.
class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final Set<WordPair> _savedFavorites = Set<WordPair>(); /// Set does not allow duplicat entries

  Widget _buildRow(WordPair pair) {
    final bool isSaved = _savedFavorites.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
          isSaved ? Icons.favorite : Icons.favorite_border,
          color: isSaved ? Colors.red : null
      ),
      onTap: () {
        setState(() {
          if (isSaved) {
            _savedFavorites.remove(pair);
          } else {
            _savedFavorites.add(pair);
          }
        });
      },
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) return Divider();

          final index = i ~/ 2;
          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        });
  }

  Widget _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _savedFavorites.map(
                (WordPair pair) {
              return ListTile(
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                ),
              );
            },
          );
          final List<Widget> divided = ListTile
              .divideTiles(
              context: context,
              tiles: tiles
          ).toList();
          return Scaffold(
            appBar: AppBar(
              title: Text('My Favorites'),
            ),
            body: ListView(children: divided),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // Step 1
    return Scaffold(
      appBar: AppBar(
        title: Text("Startup Name Generator"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.library_books), onPressed: _pushSaved)
        ],
      ),
      body: _buildSuggestions(),
    );
  }
}
